const { defineConfig } = require("cypress");

module.exports = defineConfig({
  reporter: "mochawesome",
  reporterOptions: {
    reportDir: "cypress/results",
    overwrite: true,
    html: false,
    json: true,
  },
  e2e: {
    setupNodeEvents(on, config) {},
    baseUrl: "http://127.0.0.1:8000/",
  },
});
