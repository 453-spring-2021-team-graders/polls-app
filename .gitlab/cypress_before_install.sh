# install npm requirements
npm ci >/dev/null
# Install python
apt-get update >/dev/null
export py=python3.11
apt-get install -qy $py >/dev/null
ln -s /usr/bin/$py /usr/bin/python
# Install Pipx
apt-get install -qy pipx >/dev/null
pipx ensurepath
# Install poetry, project requirements, and activate shell
pipx install poetry >/dev/null
echo PATH="/root/.local/bin:$PATH"
source ~/.bashrc
poetry install >/dev/null
poetry shell >/dev/null
source .venv/bin/activate
# Run django commands
python manage.py makemigrations >/dev/null
python manage.py migrate >/dev/null
python manage.py migrate --run-syncdb >/dev/null
python manage.py collectstatic --no-input >/dev/null
# Remove large static files that would increase the cache size
rm -rf staticfiles/fontawesomefree/js-packages
rm -rf staticfiles/fontawesomefree/svgs
du -sh staticfiles/fontawesomefree/metadata
