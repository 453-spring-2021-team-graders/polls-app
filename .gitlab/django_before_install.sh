# Install pipx
export PATH="/root/.local/bin:$PATH"
pip install --upgrade -q pip >/dev/null
apk add pipx >/dev/null
pip install -q --user pipx >/dev/null
pipx ensurepath >/dev/null
# Install poetry and project dependencies
pipx install poetry >/dev/null
poetry install >/dev/null
poetry shell >/dev/null
source .venv/bin/activate
# Run Django commands
python manage.py migrate >/dev/null
python manage.py collectstatic --no-input >/dev/null
# Remove large static files that would increase the cache size
rm -rf staticfiles/fontawesomefree/js-packages
rm -rf staticfiles/fontawesomefree/svgs
du -sh staticfiles/fontawesomefree/metadata
