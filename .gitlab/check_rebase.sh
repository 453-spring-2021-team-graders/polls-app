#!/bin/bash
# Bash script to determine if current branch(default HEAD)
# need rebase the changes from the integration
# branch(default master)
# Author:  Justin Zhang <fgz@qad.com>
# Created: 2013-12-04
#
# Usage:
#       check_rebase.sh [current branch] [integration branch]
#
set -o errexit

trg=$1
itg=$2

git fetch --all --prune

if [[ -z $trg ]]; then
    trg=HEAD
fi
if [[ -z $itg ]]; then
    itg=main
fi

ret=$(git rev-list $trg..$itg)
if [[ -z $ret ]]; then
    echo "You have no need to rebase."
    exit 0
else
    echo "You need rebase to absorb the following changes:"
    for r in $ret
    do
        echo $r
    done
    exit 1
fi
