stages:
  - lint
  - build
  - test

## Set environment variables for folders in "cache" job settings for npm modules and Cypress binary
variables:
  npm_config_cache: "$CI_PROJECT_DIR/.npm"
  CYPRESS_CACHE_FOLDER: "$CI_PROJECT_DIR/cache/Cypress"

cache:
  paths:
    - .cache/pip/
    - .venv/
    - staticfiles/
    - cache/Cypress
    - node_modules
    - build

# Defines the job in Static Analysis
django-lint:
  image: python:3.12-alpine
  stage: lint
  before_script:
    - .gitlab/django_before_install.sh
    - source .venv/bin/activate
  script:
    - ruff check .
    - ruff format .

# Defines the job in build
# Checks if migrations files are missing
django-migrations:
  image: python:3.12-alpine
  stage: build
  needs: ["django-lint"]
  before_script:
    - .gitlab/django_before_install.sh
    - source .venv/bin/activate
  script:
    - python3 manage.py makemigrations --check
    - python3 manage.py migrate --check
    - python3 manage.py check
    - python3 manage.py check --database default
    - python3 manage.py check --deploy

# Defines the job in test
django-tests:
  image: python:3.12-alpine
  stage: test
  needs: ["django-migrations", "django-lint"]
  before_script:
    - .gitlab/django_before_install.sh
    - source .venv/bin/activate
  script:
    - pytest .
    - coverage xml
    - coverage report

  coverage: '/TOTAL.*\s+(\d+%)$/'
  artifacts:
    when: always
    reports:
      junit: report.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml

cypress-e2e:
  image: cypress/base:22.11.0
  stage: test
  before_script:
    - .gitlab/cypress_before_install.sh
    - source .venv/bin/activate
  script:
    # start the server in the background
    - python manage.py testserver cypress/fixtures/testdb.json --no-input &
    # run Cypress tests
    - npm run e2e:headless
  artifacts:
    when: always
    paths:
      - cypress/results/mochawesome.json
