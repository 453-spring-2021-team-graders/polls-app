**Table of Contents**

[[_TOC_]]

# Django Polls App
The project is deployed on [Render](https://polls-app-2iql.onrender.com/).

To develop/test this website, clone this repository and follow the instructions:

## Prerequisites

Ensure you have the following software already installed:
- [![python][python-badge]][python-url]
- [![NodeJs][node-badge]][node-url]

Then clone the [Polls App](https://gitlab.com/ull-gitlab/453-spring-2021-team-graders/polls-app) repo.

**Note:** SSH-based clone is recommended.

### Install Python Requirements
[![Poetry][poetry-badge]][poetry-url]

- Install `pipx` using any of the options in the [official pipx installation instructions](https://pipx.pypa.io/stable/installation/) or the following commands:
   ```bassh
   python3 -m pip install --user pipx
   python3 -m pipx ensurepath
   ```
- Install `poetry`:
   ```bash
   pipx install poetry
   ```
- Install Django dependencies defined in `pyproject.toml` using `poetry`:
   ```bash
   poetry install
   ```
   This command also creates a python virtual environment in `.venv` directory.

- Activate the virtual environment:
   ```bash
   source .venv/bin/activate
   ```
   or
   ```bash
   poetry shell
   ```

### Install and Set Up Pre-commit Hooks
**NOTE**: To run the pre-commit hooks, the python virtual environment needs to be active. Hence, it is recommended to create git commits and pushes ONLY from the shell with venv activated.

- Install and setup `pre-commit` hooks:

   [![pre-commit][pre-commit-badge]][pre-commit-url]
   [![commitizen][commitzen-badge]][commitizen-url]

   The `.pre-commit-config.yaml` file contains the `pre-commit` configuration. Install `pre-commit` to the virtual environment like this
   ```bash
   (.venv) $ pre-commit install
   ```

    **Notice:** It is recomended to use VSCode for the development of this project.

- Open VSCode from the directory where the ```.vscode``` directory can be found (_always requires the virtual environment to be active in the shell while opening VSCode_).
   ```bash
   (.venv) $ code .
   ```

## Apply Migrations

```bash
(.venv) $ python manage.py migrate
```

## Collect Static Files

```bash
(.venv) $ python manage.py collectstatic --no-input
```

## Run Django Web Server

```bash
(.venv) $ python manage.py runserver
```

## Run Django Unit Tests

[![Pytest][pytest-badge]][pytest-url]

To run the Django unit tests in parallel in SQLite3 DB:
```bash
(.venv) $ python manage.py test --parallel
```
To run the Django unit tests in parallel in memory (faster):
```bash
(.venv) $ pytest .
```

## Install Cypress
[![Cypress][cypress-badge]][cypress-url]

```bash
npm install
```

This will download the cypress binaries in `node_modules` directory.


## Run Cypress E2E tests

### Run the Django TestServer
In a terminal, run the Django development server with data from the database dump or fixture (`testdb.json`):

```bash
(.venv) $ python manage.py testserver cypress/fixtures/testdb.json --no-input
```

In another terminal, run the following command:
```bash
npm run e2e
```
This command will open a browser window. Select the test as `E2E`.

If there are multiple browsers on your machine, it will prompt to you to choose a browser to run Cypress tests.

Upon choosing a browser to run Cypress tests, click on the `test.cy.js` file to start the tests.

To run the tests browser less:
```bash
npm run e2e:headless
```

## Deploy on Render
Per the guidelines provided in the class, the application is automatically deployed on Render when you push changes to the main branch.

The `build.sh` script is used to deploy the app on Render. While Render does not yet support `poetry install`, it does support `pip install`. The script uses the command `pip install -r requirements.txt` to install all the Python requirements for the application. Therefore, export the Python package requirements specified in the `pyproject.toml` files using `poetry` as follows:
```bash
poetry export --with render --without-hashes --format=requirements.txt > requirements.txt
```
The above command needs to be executed, and the resulting `requirements.txt` file needs to be added to Git whenever you add or remove a package from `pyproject.toml`.

[python-badge]: https://img.shields.io/badge/python-3.12-brightgreen?style=for-the-badge&logo=python&logoColor=green
[python-url]: https://www.python.org
[node-badge]: https://img.shields.io/badge/node-22-brightgreen?style=for-the-badge&logo=npm&logoColor=green
[node-url]: https://nodejs.org/en
[cypress-badge]: https://img.shields.io/badge/cypress-latest-brightgreen?style=for-the-badge&logo=cypress&logoColor=green
[cypress-url]: https://www.cypress.io/
[django-badge]: https://img.shields.io/badge/Django-5.1-brightgreen?style=for-the-badge&logo=django&logoColor=green
[django-url]: https://www.djangoproject.com/
[pre-commit-badge]: https://img.shields.io/badge/pre--commit-enabled-brightgreen?style=for-the-badge&logo=pre-commit&logoColor=green
[pre-commit-url]: https://github.com/pre-commit/pre-commit
[commitzen-badge]: https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=for-the-badge&logo=commitizen&logoColor=green
[commitizen-url]: https://github.com/commitizen-tools/commitizen
[poetry-badge]: https://img.shields.io/badge/poetry-enabled-brightgreen.svg?style=for-the-badge&logo=poetry&logoColor=green
[poetry-url]: https://python-poetry.org/
[pytest-badge]: https://img.shields.io/badge/pytest-latest-brightgreen?style=for-the-badge&logo=pytest&logoColor=green
[pytest-url]: https://docs.pytest.org/en/stable/
